module acidf2k4/webapi

go 1.16

require (
	github.com/gorilla/mux v1.8.0 // indirect
	gorm.io/driver/mysql v1.1.1 // indirect
	gorm.io/gorm v1.21.11 // indirect
)
