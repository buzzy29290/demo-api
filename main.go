package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func initialiserRouteur() {
	r := mux.NewRouter()

	r.HandleFunc("/utilisateurs", RapatrierUtilisateurs).Methods("GET")
	r.HandleFunc("/utilisateur/{id}", RapatrierUtilisateur).Methods("GET")
	r.HandleFunc("/utilisateurs", CreerUtilisateur).Methods("POST")
	r.HandleFunc("/utilisateur/{id}", MettreAJourUtilisateur).Methods("PUT")
	r.HandleFunc("/utilisateurs/{id}", SupprimerUtilisateur).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":9000", r))
}

func main() {
	InitiliaserMigration()
	initialiserRouteur()
}

