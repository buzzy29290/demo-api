package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var BDD *gorm.DB
var err error

//const DNS = "root:Tokkamac@2021@tcp(192.168.1.52:30001)/godb"
var adresseServeurSQL = os.Getenv("WEBAPI_ADR_SQL")
var portServeurSQL = os.Getenv("WEBAPI_PORT_SQL")
var idServeurSQL = os.Getenv("WEBAPI_USR_SQL")
var passServeurSQL = os.Getenv("WEBAPI_PASS_SQL")
var nomBDDSQL = os.Getenv("WEBAPI_BDD_SQL")

var DNS = idServeurSQL + ":" + passServeurSQL + "@tcp(" + adresseServeurSQL + ":" + portServeurSQL + ")/" + nomBDDSQL

type Utilisateur struct {
	gorm.Model
	Prenom string `json:"prenom"`
	Nom    string `json:"nom"`
	Email  string `json:"email"`
}

func InitiliaserMigration() {
	BDD, err = gorm.Open(mysql.Open(DNS), &gorm.Config{})
	if err != nil {
		fmt.Println(err.Error())
		fmt.Println(DNS)
		panic("Impossible de se connecter à la base de données")
	}
	BDD.AutoMigrate(&Utilisateur{})
}

func RapatrierUtilisateurs(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var utilisateurs []Utilisateur
	BDD.Find(&utilisateurs)
	json.NewEncoder(w).Encode(utilisateurs)
}

func RapatrierUtilisateur(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	var utilisateur Utilisateur
	BDD.First(&utilisateur, params["id"])
	json.NewEncoder(w).Encode(utilisateur)
}

func CreerUtilisateur(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var utilsateur Utilisateur
	json.NewDecoder(r.Body).Decode(&utilsateur)
	BDD.Create(&utilsateur)
	json.NewEncoder(w).Encode(utilsateur)
}

func SupprimerUtilisateur(w http.ResponseWriter, r *http.Request) {

}

func MettreAJourUtilisateur(w http.ResponseWriter, r *http.Request) {

}

