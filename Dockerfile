FROM golang:alpine AS build
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN go mod download
RUN CGO_ENABLED=0 go build -o main .

FROM scratch
COPY --from=build /app /bin/api
CMD [ "/bin/api/main" ]
EXPOSE 9000
